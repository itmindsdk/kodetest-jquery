![logo.png](https://bitbucket.org/repo/peBLyb/images/1663685387-logo.png)

Denne test indeholder 6 opgaver, som hver indeholder nogle opgaver, vi vil have du skal løse.
Testen tester dine evener i HTML, CSS, JavaScript og JQuery.
Du må ikke importerer yderligere biblioteker.

Du skal aflevere en "zipped" version af hele din løsning.
Hvis du støder ind i problemer med at forstå en opgave, så tag endelig kontakt til os.

Good luck and happy coding!

# Hvordan downloader jeg testen?
Du kan downloade projektet ved enten at klone dette git repository
eller ved at trykke "Downloads" i menuen til venstre og
derefter vælge "Download repository"

# Hvordan skal opgaven besvares?
Hver opgave skal afleveres separat. Det vil sige når du har løst opgave 1, så kopiere du opgave 1 over i en ny mappe til opgave 2 osv.
Det du ender med at aflevere er en zip fil med mapperne:

Opgave 1

Opgave 2

Opgave 3

Opgave 4

Opgave 5

Opgave 6

# Hvad bedømmes du på?
Du bliver bedømt ud fra følgende kriterier:

	- Er opgaven løst korrekt
	
	- Anvendelse af best practice
	
	- Læsbarhed
	
	- Kommentarer og overvejelser
	

# Opgaven
Pre:
En kunde har et site, som indeholder to tabeller over alle deres medlemmer. Tabellerne er opdelt efter mænd og kvinder.
Kunden oplever at der er nogle små problemer med siden, bl.a. at data ikke altid vises rigtigt.
Løsningen består af en html fil, main.html, en css fil, site.css og to javascript filer, main.js og fillTables.js. 
Kundens site er delt op i tre mapper: css, html og js.
Du må ændre i filerne main.html, site.css og main.js
Du skal ikke ændre i filen fillTables.js. 
NB: Datamængden i dette tilfælde er begrænset i størrelse. Når du løser opgaven, så tænk at der kan være meget data i tabellerne.

Opgave 1.
Kunden fortæller dig at applikationen er stoppet med at virke. Find fejlen og ret den.

Opgave 2.
Kunden har har et krav om at alle personer, som er 25 år eller ældre, ikke skal vises som standard. I følge kunden har det virket, men efter at tabellen med kvinder er kommet til, så virker det ikke længere.

Opgave 3.
Kunden har nu et nyt krav om at alle rækker hvor personen er 18 år eller ældre, skal have en rød baggrundsfarve. Derudover vil kunden også have at tabellerne er zebrastribet (rækkerne skal være skiftesvis hvide og lys grå).

Opgave 4.
Kunden vil gerne have implementeret knapper, så de skjulte personer kan blive vist. Knappen, som skal vise de skjulte personer, skal også kunne skjule de personer som er 25 år igen.
Kunden vil gerne have at knapperne "Vis skjulte" bliver mere brugervenlige, så når man har trykket på dem, ændre de funktionalitet og tekst, så de kan skjule alle rækker hvor personen er 25 år eller ældre.

Opgave 5.
Kunden vil gerne have en tabel, hvor man kan vælge personer ud.
Opret en tabel, hvor man ved dobbeltklik på de eksisterende rækker i de eksisterende tabeller kan overføre personer. En person må ikke kunne tilføjes to gange. Hvis en person tilføjes to gange skal brugeren informeres via en alert boks.
Derudover skal det være muligt at fjerne en person fra den nye tabel ved dobbelt klik. Når en person er fjernet skal det dog være muligt at tilføje dem igen.


Opgave 6.
Implementer sortering på de 3 tabeller.
Der skal være en indikator af hvilken kolonne der sorteres på og om det er ascending eller descending.(Hint: Du kan evt. bruge følgende ISO koder: \2191 og \2193)