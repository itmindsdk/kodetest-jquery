var tableService = {
    createMales: createMales,
    createFemales: createFemales,
    createTable: createTable
};

function createTable(tableId,dataArray){
 var table = document.getElementById(tableId).getElementsByTagName('tbody')[0];

    dataArray.forEach(function (element) {
        // Create an empty <tr> element and add it to the last position of the table:
        var row = table.insertRow();

        // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        // Add some text to the new cells:
        cell1.innerHTML = element.name;
        cell2.innerHTML = element.age;
        cell3.innerHTML = element.address
        cell4.innerHTML = element.postalCode
        cell5.innerHTML = element.city
    }, this);
}

function Person(name, age, address, postalCode, city) {
    this.name = name;
    this.age = age;
    this.address = address;
    this.postalCode = postalCode;
    this.city = city;
    
}

var males = [];
var females = [];

function createMales() {
    males.push(new Person("Anders Hansen", 18, "Paradisvej 16", "2800", "Kgs. Lyngby"));
    males.push(new Person("Jens Nielsen", 12, "Æblegade 122", "1614", "København V"));
    males.push(new Person("Oskar Skov", 21, "Lyngvej 8", "2850", "Nærum"));
    males.push(new Person("Henrik Henriksen", 22, "Øster Allé 29", "2750", "Ballerup"));
    males.push(new Person("Jim Gynge", 27, "Vejgade 21", "2740", "Skovlunde"));
    males.push(new Person("Martin Nielsen", 9, "Issingvej 53", "1614", "København V"));
    males.push(new Person("Daniel Larsen", 4, "Egevang 75", "1614", "København V"));
    males.push(new Person("Rumle Johanneson", 13, "Gumlevænget 101A", "2800", "Kgs. Lyngby"));
}

function createFemales() {
    females.push(new Person("Cathrine Ingrid Sørensen", 19, "Åboulevard 28", "4000", "Roskilde"));
    females.push(new Person("Louise Schou", 15, "Paradisvej 1", "4000", "Roskilde"));
    females.push(new Person("Mette Pedersen", 29, "Stationsvej 234", "3400", "Hillerød"));
    females.push(new Person("Lotte Didriksen", 17, "Vestre Landevej 9", "2800", "Kgs. Lyngby"));
    females.push(new Person("Ditte Hansen", 21, "Gammelvej 763", "2800", "Kgs. Lyngby"));
}

